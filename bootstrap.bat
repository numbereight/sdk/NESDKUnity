@echo off

set DIR=%~dp0

pushd "%DIR%\Scripts"

python build_android.py --prepare %DIR% || goto :error
python build_ios.py --prepare --no-copy-shared %DIR% || goto :error

popd
exit /b 0

:error
exit /b %errorlevel%