# NESDKUnity
This is the NumberEight Unity SDK. This allows games and apps made using Unity to have access to NumberEight Audiences, in much the same way they can for native apps using the Android and iOS SDK.

## Requirements

### Unity Editor Compatability
Currently only tested to work with: `Unity 2020.1.6f1`, `Unity 2020.1.15f1`

### System Requirements
Only supports Android and iOS projects. Must have the necessary developer tools installed for the target platform. On Android, we require `Gradle` to be installed, as well as `Android SDK >= 19`. On iOS, we require `CocoaPods` to be installed.

## Installation
AudiencesUnity is a free package. You can download it from the offical repo here: https://gitlab.com/numbereight/sdk/NESDKUnity/-/packages

In order to use the SDK, you will need to obtain a NumberEight API key, which you can get through the [portal](https://portal.eu.numbereight.ai/keys).

## Getting Started
The plugin contains an `Audiences` prefab, located in `Audiences/API/Audiences.prefab`. Place one of these prefabs in any scene in your project. When the scene starts, the NumberEight Audiences will start automatically. There are public functions available if you wish to stop or restart the Audience service.

There is a checkbox to enable/disable the Audience service. If this is disabled, the audience service will do nothing, and all functions within the class become a noop. This flag is checked whenever a function is invoked.

The API key you obtained from the portal *must* be entered into the string field or the SDK will fail to start.

You can get access to the current list of audiences through: `NE.Audiences.Audiences.CurrentMemberships()`. This returns a list of membership objects containing the current audiences the user is part of.

## Building on iOS
You need to manually embed `NumberEightCompiled.xcframework` in `Unity-iPhone`.  To do so, go into your `Unity Xcode Project` -> Change the target to `Unity-iPhone` -> `Build Phases` -> `Embed Frameworks` -> Add `NumberEightCompiled.xcframework`

## Maintainers
* [Matthew Paletta](matt@numbereight.ai)
* [Chris Watts](chris@numbereight.ai)
