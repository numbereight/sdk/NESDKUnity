﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public void ShowAdsMenu()
    {
        SceneManager.LoadScene("AdsMenu");
    }
}
