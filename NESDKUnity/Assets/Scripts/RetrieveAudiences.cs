﻿using UnityEngine;
using UnityEngine.UI;
using NE.Audiences;
using System.Linq;
using System.Collections.Generic;

public class RetrieveAudiences : MonoBehaviour
{
    private Text audiencesBox;
    private StartNumberEight starterScript;

    // Start is called before the first frame update
    void Start()
    {
        audiencesBox = gameObject.GetComponent<Text>();
        starterScript = gameObject.GetComponent<StartNumberEight>();
        InvokeRepeating("UpdateAudiences", 0, 5);
    }

    // Update is called once per frame
    void UpdateAudiences()
    {
        if (!starterScript.InitComplete)
        {
            return;
        }

        if (!starterScript.InitSuccessful)
        {
            audiencesBox.text = starterScript.ErrorMessage;
            return;
        }

        List<string> names = Audiences.CurrentMemberships()
            .FindAll(membership =>
                membership.liveness == "live"
                || membership.liveness == "habitual")
            .Select(membership => membership.name).ToList();

        if (names.Count > 0)
        {
            audiencesBox.text = string.Join("\n", names);
        }
        else if (starterScript.InitSuccessful)
        {
            audiencesBox.text = "Thinking...";
        }
    }
}
