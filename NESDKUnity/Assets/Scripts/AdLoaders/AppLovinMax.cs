﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

using NE.Audiences;
using NE.Utils;

public class AppLovinMax : AdLoader
{
    #if UNITY_ANDROID
        string bannerAdUnitId = "486a93a9218053ab";
        string interstitialAdUnitId = "0d5a3f5b06b7b6d8";
    #elif UNITY_IOS
        string bannerAdUnitId = "1f49f6debf278e0b";
        string interstitialAdUnitId = "fc493a47c477153b";
    #else
        string bannerAdUnitId = "unexpected_platform";
        string interstitialAdUnitId = "unexpected_platform";
    #endif

    public class Mediation {
        public static string Default = "Default";
        public static string GAM = "Google Ad Manager";
    }

    public override ReadOnlyCollection<string> MediationOptions {
        get {
            return new ReadOnlyCollection<string>(
                new List<string> {
                    Mediation.Default, Mediation.GAM,
                }
            );
        }
    }

    private TaskCompletionSource<bool> bannerPromise = null;
    private TaskCompletionSource<bool> interstitialPromise = null;

    public void Start()
    {
        // Initialize AppLovin MAX SDK
        MaxSdkCallbacks.OnSdkInitializedEvent += SetupAdUnits;

        MaxSdk.SetSdkKey("0EWIg8Z0duXdHbRESQFdq76s_TX1KofH89_bkUUxbEgrMAEhn9rL9TK1ZGMPmXc0UwCkyHL0qPTIcO21TG9WWt");
        MaxSdk.InitializeSdk();

        OnMediationNetworkUpdated += (string _) => {
            ClearTargetingData();
        };
    }

    public void OnDestroy()
    {
        MaxSdkCallbacks.OnSdkInitializedEvent -= SetupAdUnits;
    }

    private void SetupAdUnits(MaxSdkBase.SdkConfiguration sdkConfiguration)
    {
        // Create a 320x50 banner at the top of the screen.
        MaxSdk.CreateBanner(bannerAdUnitId, MaxSdkBase.BannerPosition.TopCenter);

        MaxSdk.SetBannerExtraParameter(bannerAdUnitId, "adaptive_banner", "false");
        MaxSdk.SetBannerBackgroundColor(bannerAdUnitId, Color.clear);
        MaxSdk.StopBannerAutoRefresh(bannerAdUnitId);
        MaxSdk.HideBanner(bannerAdUnitId);

        MaxSdkCallbacks.Banner.OnAdLoadedEvent += (string adUnitId, MaxSdkBase.AdInfo adInfo) =>
        {   
            if (bannerPromise?.TrySetResult(true) == true) {
                MaxSdk.ShowBanner(adUnitId);
            }
        };

        MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += (string adUnitId, MaxSdkBase.AdInfo adInfo) =>
        {
            if (interstitialPromise?.TrySetResult(true) == true)
            {
                MaxSdk.ShowInterstitial(adUnitId);
            }
        };

        MaxSdkCallbacks.Banner.OnAdLoadFailedEvent += (string adUnitId, MaxSdkBase.ErrorInfo error) =>
        {
            bannerPromise?.TrySetException(new System.Exception(error.Message));
        };

        MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += (string adUnitId, MaxSdkBase.ErrorInfo error) =>
        {
            interstitialPromise?.TrySetException(new System.Exception(error.Message));
        };
    }

    private void ClearTargetingData()
    {
        MaxSdk.TargetingData.Keywords = null;
        MaxSdk.SetBannerLocalExtraParameter(bannerAdUnitId, "custom_targeting", null);
        MaxSdk.SetInterstitialLocalExtraParameter(interstitialAdUnitId, "custom_targeting", null);
    }

    private void UpdateTargetingData()
    {
        // Collect audiences for all liveness types
        var memberships = Audiences.CurrentMemberships();
        var habitual = memberships.FindAll(m => m.liveness == "habitual");
        var live = memberships.FindAll(m => m.liveness == "live");
        var today = memberships.FindAll(m => m.liveness == "today");
        var thisWeek = memberships.FindAll(m => m.liveness == "this_week");
        var thisMonth = memberships.FindAll(m => m.liveness == "this_month");

        if (MediationNetwork == Mediation.Default)
        {
            /// Begin CodeSnippet: SetMAXTargetingData
            // For targeting with AppLovin MAX direct sales.
            // Add all audiences as keywords.
            // Note that the '-' character as in 'NE-1-1' is reserved in AppLovin MAX,
            // so they must be replaced with '_'.
            // Audiences build up over time: live audiences are available after a few seconds, whereas
            // habitual ones can take several days.
            // While testing, it is recommended to continually make new ad requests to see as many
            // audiences as possible.
            var keywords = new[] {
                habitual.Select(m => "ne_habitual:" + m.id),
                live.Select(m => "ne_live:" + m.id),
                today.Select(m => "ne_today:" + m.id),
                thisWeek.Select(m => "ne_this_week:" + m.id),
                thisMonth.Select(m => "ne_this_month:" + m.id)
            }.SelectMany(group => 
                group.Select(keyword => keyword.Replace("-", "_"))
            );
            MaxSdk.TargetingData.Keywords = keywords.ToArray();
            /// End CodeSnippet: SetMAXTargetingData
        }
        else if (MediationNetwork == Mediation.GAM)
        {
            /// Begin CodeSnippet: SetMAXGAMTargetingData
            // For targeting in Google Ad Manager.
            // Add all audiences as LocalExtraParameters to be added to CustomTargeting options.
            // Audiences build up over time: live audiences are available after a few seconds, whereas
            // habitual ones can take several days.
            // While testing, it is recommended to continually make new ad requests to see as many
            // audiences as possible.
            var customTargeting = new Dictionary<string, string>()
            {
                { "ne_habitual", string.Join(",", habitual.Select(m => m.id)) },
                { "ne_live", string.Join(",", live.Select(m => m.id)) },
                { "ne_today", string.Join(",", today.Select(m => m.id)) },
                { "ne_this_week", string.Join(",", thisWeek.Select(m => m.id)) },
                { "ne_this_month", string.Join(",", thisMonth.Select(m => m.id)) }
            };
            // Repeat for all ad units
            MaxSdk.SetBannerLocalExtraParameter(bannerAdUnitId, "custom_targeting", Marshalling.DictionaryToNativeMap(customTargeting));
            MaxSdk.SetInterstitialLocalExtraParameter(interstitialAdUnitId, "custom_targeting", Marshalling.DictionaryToNativeMap(customTargeting));
            /// End CodeSnippet: SetMAXGAMTargetingData
        }
    }

    public override Task LoadBanner()
    {
        return WrapPromise(ref bannerPromise, (TaskCompletionSource<bool> promise) =>
        {
            UpdateTargetingData();
            /// Begin CodeSnippet: LoadMAXBannerAd
            MaxSdk.LoadBanner(bannerAdUnitId);
            /// End CodeSnippet: LoadMAXBannerAd
        });
    }

    public override void CloseBanner()
    {
        bannerPromise?.TrySetCanceled();
        MaxSdk.HideBanner(bannerAdUnitId);
    }

    public override Task LoadInterstitial()
    {
        return WrapPromise(ref interstitialPromise, (TaskCompletionSource<bool> promise) =>
        {
            UpdateTargetingData();
            MaxSdk.LoadInterstitial(interstitialAdUnitId);
        });
    }

    public override void CancelInterstitial()
    {
        interstitialPromise?.TrySetCanceled();
    }
}
