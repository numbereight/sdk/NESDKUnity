﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UnityEngine;

public abstract class AdLoader : MonoBehaviour
{
    public virtual ReadOnlyCollection<string> MediationOptions
    {
        get
        {
            return new ReadOnlyCollection<string>(
                new List<string> { "Default" }
            );
        }
    }

    private string mediationNetwork;
    public string MediationNetwork
    {
        get
        {
            return mediationNetwork ?? MediationOptions[0];
        }
        set
        {
            if (!MediationOptions.Contains(value))
            {
                throw new ArgumentException("Unknown mediation option: " + value);
            }
            mediationNetwork = value;
            OnMediationNetworkUpdated(value);
        }
    }

    protected event Action<string> OnMediationNetworkUpdated;
    
    public abstract Task LoadBanner();
    public abstract void CloseBanner();
    public abstract Task LoadInterstitial();
    public abstract void CancelInterstitial();

    public void CloseAll()
    {
        CloseBanner();
        CancelInterstitial();
    }

    protected Task WrapPromise<T>(
        ref TaskCompletionSource<T> targetPromise,
        Action<TaskCompletionSource<T>> action)
    {
        var promise = new TaskCompletionSource<T>();
        var currentStatus = targetPromise?.Task?.Status;
        if (currentStatus != null && currentStatus < TaskStatus.RanToCompletion)
        {
            promise.TrySetCanceled();
        }
        else
        {
            targetPromise = promise;
            action(promise);
        }
        return promise.Task;
    }
}
