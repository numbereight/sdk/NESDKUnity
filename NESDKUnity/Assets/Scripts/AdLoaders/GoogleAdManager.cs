﻿using System.Linq;
using System.Threading.Tasks;

using GoogleMobileAds.Api;

using NE.Audiences;

public class GoogleAdManager : AdLoader
{
    #if UNITY_ANDROID
        string bannerAdUnitId = "/22836750855/test-mobile-banner";
        string interstitialAdUnitId = "/22836750855/test-mobile-video-interstitial";
    #elif UNITY_IPHONE
        string bannerAdUnitId = "/22836750855/test-mobile-banner";
        string interstitialAdUnitId = "/22836750855/test-mobile-video-interstitial";
    #else
        string bannerAdUnitId = "unexpected_platform";
        string interstitialAdUnitId = "unexpected_platform";
    #endif

    private BannerView bannerView;

    private TaskCompletionSource<bool> bannerPromise = null;
    private TaskCompletionSource<bool> interstitialPromise = null;

    public void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(initStatus => { });
        SetupAdUnits();
    }

    private void SetupAdUnits()
    {
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(bannerAdUnitId, AdSize.Banner, AdPosition.Top);
        bannerView.OnBannerAdLoaded += () =>
        {
            if (bannerPromise?.TrySetResult(true) == true)
            {
                bannerView.Show();
            }
        };
        bannerView.OnBannerAdLoadFailed += (LoadAdError error) =>
        {
            bannerPromise?.TrySetException(new System.Exception(error.GetMessage()));
        };
    }

    private AdRequest GenerateRequest()
    {
        /// Begin CodeSnippet: CollectMemberships
        // Collect audiences for all liveness types
        var memberships = Audiences.CurrentMemberships();
        var habitual = memberships.FindAll(m => m.liveness == "habitual");
        var live = memberships.FindAll(m => m.liveness == "live");
        var today = memberships.FindAll(m => m.liveness == "today");
        var thisWeek = memberships.FindAll(m => m.liveness == "this_week");
        var thisMonth = memberships.FindAll(m => m.liveness == "this_month");
        /// End CodeSnippet: CollectMemberships

        /// Begin CodeSnippet: MakeGAMRequest
        // Create an ad request filled with NumberEight Audiences Taxonomy IDs
        // Audiences build up over time: live audiences are available after a few seconds, whereas
        // habitual ones can take several days.
        // While testing, it is recommended to continually make new ad requests to see as many
        // audiences as possible.
        var request = new AdRequest.Builder()
            .AddExtra("ne_habitual", string.Join(",", habitual.Select(m => m.id)))
            .AddExtra("ne_live", string.Join(",", live.Select(m => m.id)))
            .AddExtra("ne_today", string.Join(",", today.Select(m => m.id)))
            .AddExtra("ne_this_week", string.Join(",", thisWeek.Select(m => m.id)))
            .AddExtra("ne_this_month", string.Join(",", thisMonth.Select(m => m.id)))
            .Build();
        /// End CodeSnippet: MakeGAMRequest

        return request;
    }

    public override Task LoadBanner()
    {
        return WrapPromise(ref bannerPromise, (TaskCompletionSource<bool> promise) =>
        {
            bannerView.Hide();

            var request = GenerateRequest();
            /// Begin CodeSnippet: LoadGAMBannerAd
            bannerView.LoadAd(request);
            /// End CodeSnippet: LoadGAMBannerAd
        });
    }

    public override void CloseBanner()
    {
        bannerPromise?.TrySetCanceled();
        bannerView.Hide();
    }

    public override Task LoadInterstitial()
    {
        return WrapPromise(ref interstitialPromise, (TaskCompletionSource<bool> promise) =>
        {
            var request = GenerateRequest();
            InterstitialAd.Load(interstitialAdUnitId, request, (InterstitialAd ad, LoadAdError error) =>
            {
                if (error != null)
                {
                    promise.TrySetException(new System.Exception(error.GetMessage()));
                    return;
                }

                if (promise.TrySetResult(true))
                {
                    ad.Show();
                }
            });
        });
    }

    public override void CancelInterstitial()
    {
        interstitialPromise?.TrySetCanceled();
    }
}
