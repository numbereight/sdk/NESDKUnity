﻿using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AdsMenu : MonoBehaviour
{
    private static string LOADING_MESSAGE = "Loading...";
    private static string LOADING_FAILED_MESSAGE = "Load Failed";

    public AdLoader[] loaders;
    private AdLoader loader;
    
    private Dropdown adNetworkSelect;
    private Dropdown mediationSelect;
    private Text errorText;
    private Button bannerBtn;
    private Button i10lBtn;
    
    public void Awake()
    {
        adNetworkSelect = GameObject.Find("Ad Network").GetComponent<Dropdown>();
        mediationSelect = GameObject.Find("Mediated Network").GetComponent<Dropdown>();
        errorText = GameObject.Find("Data Text").GetComponent<Text>();
        bannerBtn = GameObject.Find("Load Banner").GetComponent<Button>();
        i10lBtn = GameObject.Find("Load Interstitial").GetComponent<Button>();
    }

    public void Start()
    {
        SelectNetwork(adNetworkSelect.value);
    }
    
    public void Return()
    {
        ClearAll();
        SceneManager.LoadScene("MainMenu");
    }
    
    public void SelectNetwork(int index)
    {
        ClearAll();
        loader = loaders[index];
        mediationSelect.options = (loader.MediationOptions.Select((string opt) => {
            return new Dropdown.OptionData(opt);
        }).ToList<Dropdown.OptionData>());
    }

    public void SelectMediationNetwork(int index)
    {
        var loader = this.loader;
        if (loader) {
            loader.MediationNetwork = loader.MediationOptions[index];
        }
    }

    private async void ButtonHandler(Button btn, Task task)
    {
        var btnText = btn.GetComponentInChildren<Text>();
        var originalText = btnText.text;
        
        try {
            btn.interactable = false;
            btnText.text = LOADING_MESSAGE;
            await task;
            errorText.text = null;
        } catch (System.Exception ex) {
            btnText.text = LOADING_FAILED_MESSAGE;
            errorText.text = ex.Message;
            await Task.Delay(2000);
        } finally {
            btn.interactable = true;
            btnText.text = originalText;
        }
    }
    
    public void LoadBanner()
    {
        ButtonHandler(bannerBtn, loader?.LoadBanner());
    }

    public void LoadInterstitial()
    {
        ButtonHandler(i10lBtn, loader?.LoadInterstitial());
    }

    public void ClearAll()
    {
        foreach (var loader in loaders) {
            loader.CloseAll();
        }
    }
}
