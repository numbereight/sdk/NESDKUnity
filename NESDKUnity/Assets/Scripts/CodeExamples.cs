﻿using UnityEngine;
/// Begin CodeSnippet: ImportAudiences
using NE.Audiences;
/// End CodeSnippet: ImportAudiences
/// Begin CodeSnippet: ImportInsights
using NE.Insights;
/// End CodeSnippet: ImportInsights
using System.Collections.Generic;
using System.Linq;

public class CodeExamples
{
    /// Begin CodeSnippet: InitializeAudiences
    class AudiencesStartListener : NE.Audiences.OnStartListener
    {
        public AudiencesStartListener() : base() {}

        public override void OnSuccess()
        {
            // Ready!
        }

        public override void OnError(string message)
        {
            Debug.LogError(message);
        }
    }

    void StartAudiences()
    {
        Audiences.SetApiKey("REPLACE_WITH_DEVELOPER_KEY");
        Audiences.StartRecording(new AudiencesStartListener());
    }
    /// End CodeSnippet: InitializeAudiences

    /// Begin CodeSnippet: InitializeInsights
    class InsightsStartListener : NE.Insights.OnStartListener
    {
        public InsightsStartListener() : base() { }

        public override void OnSuccess()
        {
            // Ready!
        }

        public override void OnError(string message)
        {
            Debug.LogError(message);
        }
    }

    void StartInsights()
    {
        Insights.SetApiKey("REPLACE_WITH_DEVELOPER_KEY");
        Insights.StartRecording(new InsightsStartListener());
    }
    /// End CodeSnippet: InitializeInsights

    void StartWithDeviceId()
    {
        string customIDFV = "00000000-0000-0000-0000-000000000000";
        /// Begin CodeSnippet: AudiencesSpecifyDeviceID
        /// Begin CodeSnippet: InsightsSpecifyDeviceID
        // Create a recording config with a custom device ID
        // Note that this will make NumberEight's data personally identifiable
        // so appropriate privacy safeguards should be put in place to support
        // users' data subject rights.
        RecordingConfig config = Insights.defaultRecordingConfig();
        config.deviceId = customIDFV;

        Insights.StartRecording(config);
        /// End CodeSnippet: InsightsSpecifyDeviceID
        Audiences.StartRecording();
        /// End CodeSnippet: AudiencesSpecifyDeviceID
    }

    void StartWithConfig()
    {
        /// Begin CodeSnippet: InsightsSpecifyConfig
        RecordingConfig config = Insights.defaultRecordingConfig();

        // Add the Device Movement context to the list of recorded topics
        config.topics.Add("motion/device_movement");

        // Put a smoothing filter on the Device Movement context
        string filter = "average:1m,8s|uniq";
        config.filters.Add("motion/device_movement", filter);

        Insights.StartRecording(config);
        /// End CodeSnippet: InsightsSpecifyConfig
    }

    void GetAudiences()
    {
        /// Begin CodeSnippet: GetAudiences
        List<Membership> memberships = Audiences.CurrentMemberships();
        Debug.Log("Audiences: " + string.Join(", ", memberships.Select(membership => membership.name)));
        /// End CodeSnippet: GetAudiences
    }

    /// Begin CodeSnippet: InsightsMarkerEvents
    void PurchaseMade(int value)
    {
        Insights.AddMarker("in_app_purchase");
    }

    void SongPlayed(string title, string artist, string genre)
    {
        Insights.AddMarker("song_played");
    }
    /// End CodeSnippet: InsightsMarkerEvents

    void DeleteUserData()
    {
        // These two functions both achieve the same result

        /// Begin CodeSnippet: DeleteUserData
        Audiences.DeleteUserData();
        /// End CodeSnippet: DeleteUserData
        /// Begin CodeSnippet: DeleteUserDataInsights
        Insights.DeleteUserData();
        /// End CodeSnippet: DeleteUserDataInsights
    }
}
