﻿using UnityEngine;
using NE.Audiences;
using NE.Insights;

public class StartNumberEight : MonoBehaviour
{
    class AudiencesStartListener : NE.Audiences.OnStartListener
    {
        StartNumberEight parent;

        public AudiencesStartListener(StartNumberEight parent) : base()
        {
            this.parent = parent;
        }

        public override void OnSuccess()
        {
            parent.initComplete = true;
        }

        public override void OnError(string message)
        {
            parent.errorMessage = message;
            parent.initComplete = true;
        }
    }

    private bool initComplete = false;
    private string errorMessage = null;

    public bool InitComplete
    {
        get { return initComplete; }
    }

    public bool InitSuccessful
    {
        get { return initComplete && errorMessage == null; }
    }

    public string ErrorMessage
    {
        get { return errorMessage; }
    }

    void Start()
    {
        var startListener = new AudiencesStartListener(this);

        // Record any Audiences errors
        Audiences.StartRecording(startListener);

        // Ignore any Insights errors
        Insights.StartRecording();
    }
}
