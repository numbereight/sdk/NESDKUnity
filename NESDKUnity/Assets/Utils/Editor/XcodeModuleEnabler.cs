﻿#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace NE.Utils {
    public class XcodeBuildConfigUpdating {
        [PostProcessBuildAttribute(3)]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
            if (target != BuildTarget.iOS) {
                return;
            }

            string projPath = Path.Combine(pathToBuiltProject, "Unity-iPhone.xcodeproj/project.pbxproj");
            PBXProject proj = new PBXProject();
            proj.ReadFromFile(projPath);

#if UNITY_2019_3_OR_NEWER
            var appTarget = proj.GetUnityFrameworkTargetGuid();
            AddNEPlist(proj, proj.GetUnityMainTargetGuid(), pathToBuiltProject);
#else
            var appTarget = proj.TargetGuidByName(PBXProject.GetUnityTargetName());
            AddNEPlist(proj, appTarget, pathToBuiltProject);
#endif

            File.WriteAllText(projPath, proj.WriteToString());
        }

        public static void AddNEPlist(PBXProject proj, string target, string path) {
            if (File.Exists(target))
            {
                // Copy plist from the project folder to the build folder
                FileUtil.ReplaceFile("Assets/NumberEight-Info.plist", path + "/NumberEight-Info.plist");
                proj.AddFileToBuild(target, proj.AddFile("NumberEight-Info.plist", "NumberEight-Info.plist"));
            }
        }
    }
}

#endif // UNITY_IOS
