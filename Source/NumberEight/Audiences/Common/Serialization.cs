namespace NE.Audiences
{
    [System.Serializable]
    public class MembershipArray
    {
        public Membership[] memberships;
    }

    [System.Serializable]
    public class Membership
    {
        public string id;
        public string name;
        public IABAudience[] iabIds;
        public string liveness;
    }

    [System.Serializable]
    public class IABAudience
    {
        public string id;
        public string[] extensions;
    }
}
