using System;
using UnityEngine;
using System.Collections.Generic;

namespace NE.Insights
{

    // Based on: https://answers.unity.com/questions/460727/how-to-serialize-dictionary-with-unity-serializati.html
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<TKey> keys = new List<TKey>();

        [SerializeField]
        private List<TValue> values = new List<TValue>();

        // save the dictionary to lists
        public void OnBeforeSerialize()
        {
            keys.Clear();
            values.Clear();
            foreach (KeyValuePair<TKey, TValue> pair in this)
            {
                keys.Add(pair.Key);
                values.Add(pair.Value);
            }
        }

        // load dictionary from lists
        public void OnAfterDeserialize()
        {
            this.Clear();

            if (keys.Count != values.Count)
            {
                throw new System.Exception(string.Format("there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable."));
            }

            for (int i = 0; i < keys.Count; i++)
            {
                this.Add(keys[i], values[i]);
            }
        }
    }

    [Serializable]
    public class DictionaryOfStringAndString : SerializableDictionary<string, string> {}

    [Serializable]
    public class RecordingConfig
    {
        /**
         * A unique identifier for the device or user.
         * This could be a random UUID, or an account ID for example.
         * This is required only for analysing habitual behaviour of users.
         *
         * @note If this field is used, all data sent to NumberEight must be treated as
         * personal information, and it should be clearly communicated in a privacy notice.
         *
         * Default: null
        */
        public string deviceId;

        /**
         * If true, uploads will only take place on non-metered connections, i.e. Wi-Fi.
         *
         * If a cellular connection is determined to be non-metered, then it may also be used
         * when this flag is set to true.
         *
         * Default: false.
         */
        public bool uploadWithWifiOnly;

        /**
         * The initial delay before uploading the first samples to NumberEight.
         * This should ideally be short enough to trigger at least one upload even if the user
         * does not keep the app open for very long.
         *
         * Default: 3s
         */
        public double initialUploadDelay;

        /**
         * The interval at which data will be sent to NumberEight after the initial upload.
         *
         * Default: 5m
         */
        public double uploadInterval;

        /**
         * Upload only the most probable context of each glimpse.
         * If false, the full glimpse of all possible contexts is uploaded.
         */
        public bool mostProbableOnly;

        /**
         * A set of topics to record. Defaults to basic high-level information suitable for
         * most use cases.
         *
         * Default:
         *  "activity"
         *  "device_position"
         *  "indoor_outdoor"
         *  "reachability"
         *  "situation"
         *  "place"
         *  "time"
         *  "weather"
         */
        public List<string> topics = new List<string>();

        /**
         * A map of filters for advanced uses. The key should be an element from the set of topics,
         * and the value should be a valid NumberEight Engine filter string.
         *
         * The most common use case is for averaging.
         *
         * For example, "avg:30s" for a 30 second rolling average.
         *
         * Default: basic smoothing, unique glimpses only.
         */
        public DictionaryOfStringAndString filters = new DictionaryOfStringAndString();

        /**
         * Creates a default config.  Suitable for most use cases.
         */
        public static RecordingConfig defaultConfig()
        {
            // Pass through to the other implementation.
            // Insights has access to the plugin, but this makes more sense here as a public API.
            return Insights.defaultRecordingConfig();
        }
    }
}
