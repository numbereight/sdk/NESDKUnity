﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_IOS
    using System.Runtime.InteropServices; // Includes DllImport
#endif

namespace NE.Utils {
    class Marshalling {
        #if UNITY_IOS
            [DllImport("__Internal")]
            private static extern IntPtr convertJsonToNSDictionary(string json);
        #endif

        public static string DictionaryToJson(IDictionary<string, string> dict)
        {
            var keys = dict.Select(d => string.Format("\"{0}\":\"{1}\"", d.Key, d.Value));
            return "{" + string.Join(",", keys) + "}";
        }
        
        public static object DictionaryToNativeMap(IDictionary<string, string> dict)
        {
            #if UNITY_ANDROID
                var map = new AndroidJavaObject("java.util.HashMap");
                var putMethod = AndroidJNIHelper.GetMethodID(map.GetRawClass(), "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
                foreach (var entry in dict) {
                    AndroidJNI.CallObjectMethod(
                        map.GetRawObject(),
                        putMethod,
                        AndroidJNIHelper.CreateJNIArgArray(new object[] { entry.Key, entry.Value })
                    );
                }
                return map;
            #elif UNITY_IOS
                var json = DictionaryToJson(dict);
                return convertJsonToNSDictionary(json);
            #else
                return dict;
            #endif
        }
    }
}