#!/usr/bin/env python3
from constants import *
import os
import sys
import argparse
from distutils.dir_util import copy_tree
from shutil import copyfile, rmtree
import subprocess
from typing import List


def sdk_name():
    return "NESDKUnity"


def demo_name():
    return "NESDKUnityDemo"


def get_unity_method_call(project_name, method):
    return get_unity_call(project_name) + ["-executeMethod", method]


def get_unity_platform_build(project_name, platform):
    return get_unity_call(project_name) + ["-buildTarget", platform]


def get_unity_export(project_name: str, exports: List[str], path: str):
    return (
        get_unity_call(project_name)
        + ["-exportPackage"]
        + list(map(lambda e: '""' + e + '""', exports))
        + [path]
    )
    
def get_unity_import(project_name: str, path: str):
    return (
        get_unity_call(project_name)
        + ["-ignorecompilererrors", "-importPackage", path]
    )


def get_unity_call(project_name):
    # There is a bug is Unity 2019 when downgrading assets and it gets stuck
    assembly_updater_flag = "-disable-assembly-updater" if unity_version.startswith("2019") else ""
    return [
        unity_bin,
        "-quit",
        "-batchmode",
        "-nographics",
        assembly_updater_flag,
        "-projectPath",
        safe_rel_path(project_name),
        "-logfile",
    ]


def safe_path(arg, *args):
    if len(args) == 0:
        return arg

    san_f = lambda a: a.replace("\\", os.sep).replace("/", os.sep).replace(" ", "\ ")
    san_arg = san_f(arg)
    san_args = list(map(san_f, args))
    is_compound_path = os.sep in arg

    return os.path.join(arg, *args)


def safe_rel_path(*args):
    return safe_path(curr_dir(), *args)


def safe_call(f):
    print("Running (" + " ".join(f) + ") in dir (" + curr_dir() + ")")
    subprocess.check_call(" ".join(f), cwd=curr_dir(), shell=True)


def run_unity_method(project_name, method):
    return safe_call(get_unity_method_call(project_name, method))

def run_unity_export(exports: List[str], path: str):
    return safe_call(get_unity_export(sdk_name(), exports, path))
    
def run_unity_import(project_name: str, path: str):
    return safe_call(get_unity_import(project_name, path))


def build_unity_platform(project_name, platform):
    return safe_call(get_unity_platform_build(project_name, platform))


def clear_plugins_shared(project_name):
    print("\n" + project_name + ": Clearing existing shared assets")
    rmtree(safe_rel_path(project_name, "Assets", "NumberEight"), ignore_errors=True)
    rmtree(safe_rel_path(project_name, "Assets", "ExternalDependencyManager"), ignore_errors=True)


def copy_plugins_shared(project_name):
    print("\n" + project_name + ": Copying shared Unity assets")
    copy_tree(safe_rel_path("Source"), safe_rel_path(project_name, "Assets"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Common utilities to administer the Unity projects.')
    parser.add_argument('project_dir',
                        help='directory in which the Unity projects are located')
    parser.add_argument('--clean', dest='clean', action='store_true',
                        help='removes any NumberEight files from the given project directory')
    parser.add_argument('--import', dest='import_package',
                        help='package to import into the Unity project')

    args = parser.parse_args()

    push_dir(args.project_dir)

    if args.clean:
      clear_plugins_shared(sdk_name())
    
    if args.import_package:
      run_unity_import(sdk_name(), safe_rel_path(args.import_package))

    pop_dir() # project_dir

