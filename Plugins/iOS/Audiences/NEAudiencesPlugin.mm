#import <Foundation/Foundation.h>

#import <Audiences/Audiences.h>
#import <NumberEightCompiled/NumberEightCompiled.h>

#include "NEPluginShared.h"
#include <string>
#include <sstream>

static NSString* const LOG_TAG = @"NEAudiencesPlugin";

namespace {
    static char* audiencesSetToStr(NSSet<NSString*>* set) {
        NSString* audiencesStr = [set.allObjects componentsJoinedByString:@","];
        return convertNSStringToCString(audiencesStr);
    }
}

extern "C" {
    typedef void (*SuccessDelegate)(void* context);
    typedef void (*ErrorDelegate)(char* message, void* context);

    void NEAudiences_startRecording(const char* _Nonnull apiKey,
            SuccessDelegate _Nullable onSuccess, ErrorDelegate _Nullable onError, void* _Nullable delegateContext) {
        [NELog msg:LOG_TAG info:@"Starting NumberEight Audiences Recording"];

        // Using a long-winded approach as Audiences does not currently report errors from NumberEight.start
        NSCondition* condition = [[NSCondition alloc] init];
        __block NSString* pendingError = nil;
        __block bool ready = false;

        [condition lock];

        NSString* nsApiKey = convertCStringToNSString(apiKey);
        NEXAPIToken* token = [NEXNumberEight startWithApiKey:nsApiKey
                                               launchOptions:nil
                                              consentOptions:[NEXConsentOptions useConsentManager]
                               facingAuthorizationChallenges:^(NEXAuthorizationSource authSource, id<NEXAuthorizationChallengeResolver> _Nonnull resolver) {
            switch (authSource) {
            case kNEXAuthorizationSourceLocation:
                // Location permission requests are ignored - we get what we're given if we're given it
                //[resolver requestAuthorization];
                break;
            }
        }
                                                  completion:^(BOOL success, NSError * _Nullable error) {
            if (success) {
                [NELog msg:LOG_TAG info:@"NumberEight has started"];
            } else {
                [NELog msg:LOG_TAG error:[NSString stringWithFormat:@"Failed to start NumberEight: %@", error.debugDescription]];
                // In v3.7, there's a bug where the error can be nil in certain conditions
                if (error != nil) {
                    pendingError = error.debugDescription;
                } else {
                    pendingError = @"NumberEight was unable to start - please check your consent options and API key.";
                }
            }
            ready = true;
            [condition signal];
            [condition unlock];
        }];

        [NEXAudiences startRecordingWithApiToken:token
                                         onStart:^(NSError * _Nullable error) {
            // In v3.7 this is called on the same thread as startRecording
            // Waiting in a new thread instead
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [condition lock];
                while (!ready) {
                    [condition wait];
                }
                [condition unlock];

                if (error != nil) {
                    [NELog msg:LOG_TAG error:[NSString stringWithFormat:@"Failed to start Audiences: %@", error.debugDescription]];
                    if (onError != nil) {
                        onError(convertNSStringToCString(error.debugDescription), delegateContext);
                    }
                    return;
                } else if (pendingError != nil) {
                    [NELog msg:LOG_TAG error:[NSString stringWithFormat:@"Failed to start Audiences due to an error in NumberEight: %@", pendingError]];
                    if (onError != nil) {
                        onError(convertNSStringToCString(pendingError), delegateContext);
                    }
                    return;
                }

                [NELog msg:LOG_TAG info:@"Audiences has started"];
                if (onSuccess != nil) {
                    onSuccess(delegateContext);
                }
            });
        }];
    }

    void NEAudiences_stopRecording() {
        [NELog msg:LOG_TAG info:@"Stopping NumberEight Audience Recording"];
        [NEXAudiences stopRecording];
    }

    void NEAudiences_pauseRecording() {
        [NELog msg:LOG_TAG info:@"Pausing NumberEight Audiences Recording"];
        [NEXAudiences pauseRecording];
    }

    char* NEAudiences_currentMemberships() {
        NSSet<NEXMembership*>* membershipsSet = [NEXAudiences currentMemberships];

        // Can't encode Audiences Membership object to JSON directly because of limitations with Apple's
        // JSONEncoder for Objective-C. Instead, we convert each object to a dictionary of plain types,
        // and serialize that.
        NSMutableArray* memberships = [NSMutableArray new];
        for (NEXMembership* member in membershipsSet.allObjects) {
            [memberships addObject:[member asDictionary]];
        }

        // Value type is an array of the encoded dictionaries, from NEXMembership.
        NSDictionary<NSString*, NSArray<NSDictionary*>*>* dict = @{@"memberships": memberships};

        NSError* error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];

        if (!jsonData) {
            [NELog msg:LOG_TAG warning:[NSString stringWithFormat:@"NumberEight Audiences encountered an error while fetching audiences: %@", error]];
            return convertNSStringToCString(@"{\"memberships\": []}");
        }
        NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return convertNSStringToCString(jsonString);
    }

    char* NEAudiences_currentIds() {
        return audiencesSetToStr([NEXAudiences currentIds]);
    }

    char* NEAudiences_currentExtendedIds() {
        return audiencesSetToStr([NEXAudiences currentExtendedIds]);
    }

    char* NEAudiences_currentIabIds() {
        return audiencesSetToStr([NEXAudiences currentIabIds]);
    }

    char* NEAudiences_currentExtendedIabIds() {
        return audiencesSetToStr([NEXAudiences currentExtendedIabIds]);
    }

    char* NEAudiences_deviceId() {
        NSString* deviceId = [NEXNumberEight deviceID];
        return convertNSStringToCString(deviceId);
    }

    void NEAudiences_deleteUserData() {
        [NELog msg:LOG_TAG info:@"Deleting user data"];
        [NEXNumberEight deleteUserData];
    }

    int NEAudiences_yearOfBirth() {
        auto yearOfBirth = [NEXAudiences yearOfBirth];
        if (yearOfBirth) {
            return yearOfBirth.intValue;
        } else {
            return 0;
        }
    }

    char* NEAudiences_gender() {
        auto gender = [NEXAudiences gender];
        if (gender) {
            return convertNSStringToCString(gender);
        } else {
            return nullptr;
        }
    }
}
