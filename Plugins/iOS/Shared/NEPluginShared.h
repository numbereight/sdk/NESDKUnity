#import <Foundation/Foundation.h>

extern "C" {
    char* _Nullable convertNSStringToCString(const NSString* _Nullable nsString);
    NSString* _Nullable convertCStringToNSString(const char* _Nullable string);
    NSString* _Nullable convertCStringToNSStringOrDefault(const char* _Nullable string, const char* _Nonnull def);
    NSDictionary* _Nullable convertJsonToNSDictionary(const char* _Nonnull jsonData);
    NSString* _Nullable convertNSDictionaryToJson(NSDictionary* _Nonnull dict);
}
