#include "NEPluginShared.h"

// From: https://stackoverflow.com/questions/37047781/how-to-return-string-from-native-ios-plugin-to-unity
char* _Nullable convertNSStringToCString(const NSString* _Nullable nsString) {
    if (nsString == NULL) {
        return NULL;
    }
    
    const char* nsStringUtf8 = [nsString UTF8String];
    // create a null terminated C string on the heap so that our string's memory isn't wiped out right after method's return
    char* cString = (char*)malloc(strlen(nsStringUtf8) + 1);
    strcpy(cString, nsStringUtf8);
    
    return cString;
}

NSString* _Nullable convertCStringToNSString(const char* _Nullable string) {
    if (string && strcmp(string, "") != 0) {
        return [NSString stringWithCString:string encoding:NSUTF8StringEncoding];
    }
    
    return nil;
}

NSString* _Nullable convertCStringToNSStringOrDefault(const char* _Nullable string, const char* _Nonnull def) {
    if (auto converted = convertCStringToNSString(string)) {
        return converted;
    }
    
    return convertCStringToNSString(def);
}

NSDictionary* _Nullable convertJsonToNSDictionary(const char* _Nonnull jsonData) {
    NSString* str = convertCStringToNSString(jsonData);
    return [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding]
                                           options:kNilOptions
                                             error:nil];
}

NSString* _Nullable convertNSDictionaryToJson(NSDictionary* _Nonnull dict) {
    NSData* data = [NSJSONSerialization dataWithJSONObject:dict
                                                   options:0
                                                     error:nil];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}
