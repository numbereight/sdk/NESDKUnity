package ai.numbereight.audiencesunity;

import android.content.Context;
import android.os.ConditionVariable;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Collection;
import java.util.Set;

import ai.numbereight.audiences.Audiences;
import ai.numbereight.audiences.Membership;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.common.Log;

@SuppressWarnings("unused")
class AudiencesPlugin {
    private static final String LOG_TAG = "AudiencesPlugin";

    private static @NotNull String join(
            @NotNull CharSequence delim,
            @NotNull Collection<String> elements
    ) {
        if (elements.size() == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (CharSequence c : elements) {
            sb.append(c);
            sb.append(delim);
        }
        int length = sb.length();
        sb.delete(length - delim.length(), length);
        return sb.toString();
    }

    void startRecording(
            @NotNull Context context,
            @NotNull String apiKey,
            @Nullable OnStartListener callback
    ) {
        Log.i(LOG_TAG, "Starting NumberEight Audiences Recording");

        // Using a long-winded approach as Audiences does not currently report errors from
        // NumberEight.start (as of v3.7)
        final ConditionVariable ready = new ConditionVariable();
        final Exception[] pendingException = { null };

        NumberEight.APIToken token = NumberEight.start(
            apiKey,
            context,
            ConsentOptions.useConsentManager(),
            new NumberEight.OnStartListener() {
                @Override
                public void onFailure(@NotNull Exception ex) {
                    Log.e(LOG_TAG, "Failed to start NumberEight", ex);
                    pendingException[0] = ex;
                }
                @Override
                public void onSuccess() {
                    ready.open();
                }
            }
        );
        Audiences.startRecording(
            token,
            new NumberEight.OnStartListener() {
                @Override
                public void onFailure(@NotNull Exception ex) {
                    Log.e(LOG_TAG, "Failed to start Audiences", ex);
                    if (callback != null) {
                        callback.OnError(ex.getMessage());
                    }
                }
                @Override
                public void onSuccess() {
                    // In v3.7 this is called on the same thread as startRecording
                    // Waiting in a new thread instead
                    new Thread(() -> {
                        ready.block();

                        if (pendingException[0] != null) {
                            Log.e(LOG_TAG, "Failed to start Audiences due to an error in NumberEight");
                            if (callback != null) {
                                callback.OnError(pendingException[0].getMessage());
                            }
                            return;
                        }

                        Log.i(LOG_TAG, "Audiences has started");
                        if (callback != null) {
                            callback.OnSuccess();
                        }
                    }).start();
                }
            }
        );
    }

    void stopRecording() {
        Log.i(LOG_TAG, "Stopping NumberEight Audiences Recording");
        Audiences.stopRecording();
    }

    void pauseRecording() {
        Log.i(LOG_TAG, "Pausing NumberEight Audiences Recording");
        Audiences.pauseRecording();
    }

    @NotNull String getCurrentMemberships() {
        Set<Membership> memberships = Audiences.getCurrentMemberships();
        JSONObject output = new JSONObject();

        try {
            JSONArray array = new JSONArray();
            output.put("memberships", array);

            for (Membership membership : memberships) {
                array.put(new JSONObject(membership.serialize()));
            }
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Failed to encode memberships as JSON", ex);
        }

        return output.toString().replaceAll("iab_ids", "iabIds");
    }

    @NotNull String getCurrentIds() {
        Set<String> ids = Audiences.getCurrentIds();
        return join(",", ids);
    }

    @NotNull String getCurrentExtendedIds() {
        Set<String> ids = Audiences.getCurrentExtendedIds();
        return join(",", ids);
    }

    @NotNull String getCurrentIabIds() {
        Set<String> ids = Audiences.getCurrentIabIds();
        return join(",", ids);
    }

    @NotNull String getCurrentExtendedIabIds() {
        Set<String> ids = Audiences.getCurrentExtendedIabIds();
        return join(",", ids);
    }

    @NotNull String getDeviceId(@NotNull Context context) {
        return NumberEight.getDeviceId(context);
    }

    @Nullable String getGender() {
        return Audiences.getGender();
    }

    int getYearOfBirth() {
        try {
            int yearOfBirth = Audiences.getYearOfBirth();
            return yearOfBirth;
        } catch (NullPointerException e) {
            // it was null
            return 0;
        }
    }

    void delete(@NotNull File root) {
        File[] list = root.listFiles();

        if (list == null) return;

        for (File f : list) {
            if (f.isDirectory()) {
                delete(f);
            } else {
                String name = f.getName();
                long bytes = f.length();
                long kilobytes = (bytes / 1024);
                boolean deleted = f.delete();
                Log.e(LOG_TAG, "Deleting file " + f.getName() + " - " + kilobytes + "KB: " + (deleted ? "SUCCESS" : "FAILED"));
            }
        }
    }

    void deleteUserData(@NotNull Context context) {
        Log.i(LOG_TAG, "Deleting user data");
        File f = new File(context.getFilesDir(), "numbereight").getAbsoluteFile();
        delete(f);
    }
}
