package ai.numbereight.audiencesunity;

interface OnStartListener {
    void OnSuccess();
    void OnError(String message);
}