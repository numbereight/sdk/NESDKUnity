package ai.numbereight.insightsunity;

interface OnStartListener {
    void OnSuccess();
    void OnError(String message);
}